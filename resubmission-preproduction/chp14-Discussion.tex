\section{Discussion and Conclusion}\label{sec:discussion}

LDDMM is a popular registration method with nice mathematical properties that has successfully been applied to various applications, see, e.g.~\cite{chp14Miller2015} and references therein. 
The estimated deformation fields are guaranteed to be diffeomorphic, the deformation paths are geodesics, large displacements can be captured, and LDDMM induces a distance measure on images~\cite{chp14Beg2005}. These properties, however, come with the drawbacks of a large computational burden, both with respect to runtime and memory consumption.

Interestingly, numerical approaches for LDDMM have not been explored in great depth and the predominant solution approach is still optimize-discretize via gradient descent as proposed in~\cite{chp14Beg2005}. 
Hence, we chose to develop discretize-optimize schemes for relaxation and shooting, offering consistent numerical optimizations for the discretized energy which, as discussed in~\cite{chp14haber2007model}, cannot be guaranteed for optimze-discretize approaches such as~\cite{chp14Beg2005}. 
Formulating a discretized energy included the discretization of PDE constraints (mainly transport and scalar conservation equations), which we accomplished via an explicit fourth order Runge Kutta method as a good compromise between efficiency and accuracy. 
We also reduced memory consumption and runtime by representing velocities and transformation maps at lower spatial resolutions compared to the images. 
This is justified due to the smoothness of velocity fields and transformation maps and still allowed for high-quality registrations. 
Finally, our proposed methods can also be used with different image similarity measures and we explored the use of NGF for lung registration.

We demonstrated the characteristics of the proposed registration methods for a two-dimensional registration example of human hand radiographs. 
This experiment confirmed that the relaxation approaches yield a better image match due to more degrees of freedom compared to the shooting approach, which is much more constrained (solutions are geodesics by construction and not only at absolute convergence) and yields a smoother deformation field.

The methods were also tested on the challenging lung CT datasets provided by DIR-Lab~\cite{chp14Castillo2010,chp14Castillo2013,chp14Castillo2009}, where we integrated the NGF distance measure to align edges located at, e.g., vessels and lung boundaries well. We compared results based on the distances of 300 expert-annotated landmark pairs per inhale-exhale scan to the state-of-the-art. With an average error of 0.98\,mm on the DIR-Lab COPD datasets the relaxation approach achieves the third-best result. 
The average error is only 0.02\,mm higher than for the second-best method~\cite{chp14Vishnevskiy2017}, which is almost negligible. 
The shooting approach performs slightly worse with an average landmark distance of 1.00\,mm for the COPD datasets whilst yielding desirable, smooth transformations. 
Nevertheless, the relaxation approach may be preferable for registration of lung CT scans from full-inspiration to full-expiration due to its reduced runtime. 


In future work an extended evaluation that includes a comparison with publicly available diffeomorphic registration software packages like Deformetrica\footnote{\url{http://www.deformetrica.org}}~\cite{chp14Durrleman2014}, MRICloud\footnote{\url{https://mricloud.org/}} \cite{chp14Jain2014}, LDDMM with frame-based kernel\footnote{\url{http://www.bioeng.nus.edu.sg/cfa/brainmapping.html}}~\cite{chp14tan2016large}, FshapesTk\footnote{\url{https://github.com/fshapes/fshapesTk/}}~\cite{chp14Charlier2017} and Lagrangian LDDMM\footnote{\url{https://github.com/C4IR/FAIR.m/tree/master/add-ons/LagLDDMM}} \cite{chp14Mang2017} would be beneficial.

For fast prototyping the usage of automatic differentiation methods could be employed to derive a DO scheme from a discretized objective function. 
This has been done, e.g., in~\cite{chp14Kuehnel2017} for shape matching.
However, it is an interesting question, how the computation time competes with the dedicated methods developed in this chapter.
Automatic differentiation is also very popular in the field of machine learning.
Recently, deep learning approaches for image registration have been proposed. Yang et al.~\cite{chp14Yang2016,chp14Yang2017} developed such a method for the shooting formulation of LDDMM. 
To refine the output of the deep-learning approach it could be used as a fast initialization for a subsequent registration with our discretize-optimize approaches and/or the deep network could be trained based on our LDDMM approaches. 