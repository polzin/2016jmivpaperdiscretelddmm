%==============================================================================
\begin{section}{Background and Related Work}\label{sec:background}
%==============================================================================
LDDMM is used to automatically establish correspondences between two (or more) given datasets, which are referred to as source and target (fixed and moving or reference and template are also common names). 
The data typically consists of images, landmarks (point clouds) or surfaces. 
In the following we restrict ourselves to the registration of three-dimensional images. 
However, when considering landmarks or surfaces the main change of the presented approaches would be the data term.

Specifically, LDDMM registration estimates a time- and space-dependent velocity field $v \colon \Omega \times [0,1]\to \R^3$, which flows the source image $I^0$ such that it matches as well as possible a target image $I^1$. 
Conceptually, the data dimension and the time-horizon are arbitrary\footnote{Although the spatial dimension is arbitrary, care has to be taken to assure that the regularization is strong enough to assure diffeomorphic results.}, but for ease of presentation, we assume that the data is three-dimensional, the images $I^0$ and $I^1$ are compactly supported on a domain $\Omega\subset\R^3$, and the time interval is $[0,1]$.

Assuming all structures are visible in the source and target images, after an ideal registration $I^0(\phi_1^{-1}(\x))=I^1(\x)$ for all $\x \in \Omega$ for a transformation $\phi \colon \Omega\times[0,1]\to\R^3$. 
This is, of course, not feasible in practice, either because there is not a perfect structural correspondence or due to the presence of noise which precludes exact matching. 
Here we use the notation $\phi_t(\x) \coloneqq \phi(\x,t)$. This notation will be used for all variables depending on space and time. In particular, in this notation, the subscript is \emph{not} related to a partial derivative. 
The sought-for transformation and the velocity fields are related via $\dot{\phi}_t(\x) = v_t(\phi_t(\x)), ~\phi_0(\x) = \x$, where $v_t(\x) \coloneqq v(\x,t)$ and $\dot{\phi} = \partial_t \phi$. 
In the remainder of the chapter we will often, for the sake of a cleaner and more compact notation, omit the spatial argument and assume that equations hold for arbitrary $\x \in \Omega$. 
Furthermore, if $t$ is not specified, we assume that equations hold for all $t\in[0,1]$.

Following the work of Beg et al.~\cite{chp14Beg2005}, a solution of the registration problem is given by a minimizer of the energy
%------------------------------------------------------------------------------
\begin{equation}\label{eq:standard-LDDMM}
 \left.
   \begin{array}{ll}
  	\mathcal{E}^\textrm{Beg}(v,\phi)&\coloneqq \regularization(v) + \dissimilarity^{\mathrm{SSD}}(I^0 \circ \phi^{-1}_1,I^1) \\
    \text{s.t. }~
	&\dot{\phi}_t = v_t \circ \phi_t, ~\phi_0 = \DiffId.
	\end{array}
\right \}	
\end{equation}
%------------------------------------------------------------------------------
The regularizer, $\regularization$, enforces smoothness of the velocity fields and the distance measure (data fit) $\dissimilarity^{\mathrm{SSD}}$ is used to compute the similarity of the transformed source image $I^0 \circ \phi_1^{-1}$ at $t=1$ and the target image $I^1$. 
The distance measure for images $I$ and $J$ is $\dissimilarity^{\mathrm{SSD}}(I,J) = \frac{1}{2\sigma^2}\int_\Omega (I(\x)-J(\x))^2~\dx$ and is called Sum of Squared Differences~(SSD). 
SSD is only one of many possibilities, see, e.g.,~\cite[pp.~95]{chp14modersitzki2009}.
We discuss further details and choices of distance measures in \autoref{sec:DistanceMeasures}. 
The regularity of $v$ is encouraged by employing a differential operator $L$ in $\regularization$:  
\begin{equation}
 \label{eq:defRegularizer}
 \left . 
 \begin{array}{l}
  \regularization(v)  \coloneqq \frac{1}{2}\int_0^1 \|v_t\|_L^2~ \dt, \mathrm{ with } \\
  \|v_t\|_L^2   \coloneqq\langle Lv_t, Lv_t\rangle \coloneqq\int_{\Omega} v_t(\x)^{\top}L^{\dagger}Lv_t(\x)~\dx.
 \end{array}
\right \} 
\end{equation}
%------------------------------------------------------------------------------
$L^\dagger$ is the adjoint operator of $L$.
We use the Helmholtz operator $L\coloneqq(\gamma \; \DiffId - \alpha \Delta)^\beta$, $\alpha,~\gamma>0$ and $\beta \in \N$, which is a typical choice~\cite{chp14Beg2005,chp14Hart2009,chp14Zhang2015}. 
Here, $\Delta$ denotes the spatial vectorial Laplacian and $v_t^i,~i=1,2,3$ is the $i$-th component of $v$ at time $t$:
%
\begin{equation}
\Delta v_t(\x) \coloneqq \begin{pmatrix} \sum_{i=1}^{3}
\partial_{x_i,x_i} v_t^1(\x)  \\
\sum_{i=1}^{3}\partial_{x_i,x_i} v_t^2(\x) \\
\sum_{i=1}^{3}\partial_{x_i,x_i} v_t^3(\x) 
\end{pmatrix}.
\end{equation}
%
Standard LDDMM models follow the optimize-discretize approach, i.e., optimality conditions are computed for the continuous model through calculus of variations and the resulting optimality conditions are then discretized. 
For example, in the derivation by Beg et al.~\cite{chp14Beg2005} the variation of~\eqref{eq:standard-LDDMM} with respect to $v$ is computed resulting in optimality conditions of the form
\begin{equation}
\label{eq:OptimalityConditionsBeg1}
 \left.
   \begin{array}{ll}
    I_t &\coloneqq I^0\circ\phi_t^{-1},\\
    \lambda_t &= |\jac{\tau_t^{-1}}|\lambda_1\circ\tau_t^{-1},\\
    L^\dagger L v_t + \lambda_t\nabla I_t &=\mat{0},
	\end{array}
\right \}
\end{equation}
with initial and final conditions
\begin{equation}
\label{eq:OptimalityConditionsBeg2}
  I_0 = I^0\quad\text{and}\quad\lambda_1 = -\frac{1}{\sigma^2}\left(I^0\circ\phi_1^{-1}-I^1\right),
\end{equation}
where $\tau_t \coloneqq \tau(t)$ is the flow for the negative velocity field (with $\tau_1=\DiffId$) and $I_t$ is the transformed source image at time $t$. 
The adjoint variable $\lambda \colon \R^3 \times [0,1]  \to \R$ is initialized at $t=1$ with the negative image mismatch (also called residual, cf.~\autoref{sec:DistanceMeasures} on distance measures) scaled by the weighting factor $\frac{1}{\sigma^2} > 0$, that balances regularizer and distance measure energy. 
The spatial gradient of the image at time $t$ is referred to as $\nabla I_t$ and the Jacobian of the spatial variables of $\tau_t^{-1}$ is $\jac{\tau_t^{-1}}$. 
Note that the adjoint variable, $\lambda$, is also called the scalar momentum in the literature~\cite{chp14vialard2012} and the quantity $\lambda\nabla I=m$ is the vector-valued momentum~\cite{chp14Singh2013}. 
In fact, the optimality conditions can be written solely with respect to the vector-valued momentum, $m$, resulting in the EPDiff equation, see, e.g.,~\cite{chp14Mumford2013}. 

Computing the variation leading to~\eqref{eq:OptimalityConditionsBeg1} and~\eqref{eq:OptimalityConditionsBeg2} in the optimize-discretize framework is rather involved. 
If the interest is only in $I_t$, one alternatively can add the flow equation in form of a transport equation in Eulerian coordinates as a constraint to the optimization problem. 
This has been proposed by Borzí et al.~\cite{chp14Borzi2003} for optical flow and by Hart et al. for LDDMM~\cite{chp14Hart2009}. 
Following~\cite{chp14Hart2009} the LDDMM formulation then becomes
\begin{equation} 
\label{eq:ModelHart}
 \left.
   \begin{array}{ll}
  \mathcal{E}^\textrm{Hart}(v,I) &= \regularization(v) + \dissimilarity^{\mathrm{SSD}}(I_1, I^1),\\
  \text{s.t.}&\dot{I}_t+\nabla I_t\trp v_t =0,~I_0=I^0,
  \end{array}
\right \}
\end{equation} 
with the optimality conditions
\begin{equation}
\label{eq:OptimalityConditionsHart}
   \left.
   \begin{array}{ll}
    \dot{I}_t + \nabla I_t\trp v_t &=0,~I_0=I^0,\\
    \dot{\lambda}_t + \div( \lambda_t v_t )&=0,~\lambda_1=-\frac{1}{\sigma^2}(I_1-I^1),\\
    L^\dagger L v_t + \lambda_t\nabla I_t&= \mat{0}.
    \end{array}
\right \}
\end{equation}
The optimality conditions~\eqref{eq:OptimalityConditionsBeg1}-\eqref{eq:OptimalityConditionsBeg2} and~\eqref{eq:OptimalityConditionsHart} are the same, but~\eqref{eq:OptimalityConditionsHart} are written directly in terms of the image $I$ and the Lagrange multiplier $\lambda$. 
In practice, both formulations are solved by computing the flows $\phi$ and $\tau$ from which the solution of the transport equation (for $I$) can be obtained by interpolation\footnote{While it is possible to solve the transport equation directly for the images, cf.~\autoref{sec:IBRmodel}, it is numerically much easier to solve for $\phi$ and $\tau$ as they will be spatially smooth, cf.~\autoref{sec:MBRmodel}.}. 
The solution for the scalar conservation law/the continuity equation for $\lambda$ is obtained by interpolation and local weighting by the determinant of the Jacobian of the transformation.

The advantage of the second optimize-discretize approach \eqref{eq:ModelHart} is that the optimality conditions~\eqref{eq:OptimalityConditionsHart} are (relatively) easy to derive and retain physical interpretability. 
For example, it is immediately apparent that the adjoint to the transport equation is a scalar conservation law. 
However, great care needs to be taken to make sure all equations and their boundary conditions are consistently discretized in the optimize-discretize approach. 
For example, it cannot be guaranteed that for any given discretization the gradients computed based on the discretized optimality conditions will minimize the overall energy~\cite{chp14haber2007model}. 
In particular, these formulations require interpolation steps which can cause trouble with the optimization, because it is unclear how they should affect a discretized gradient. 
Furthermore, the flow equations can be discretized in various ways (for example by a semi-Lagrangian scheme~\cite{chp14Beg2005}, upwind schemes~\cite{chp14Hart2009}, etc.). 
But, as these discretizations are not considered part of the optimization their effect on the gradient remains unclear.

Therefore, we follow the discretize-optimize approach in image registration~\cite[pp.~12]{chp14modersitzki2009} which starts out with a discretization of the energy to be minimized and derives the optimality conditions from this discretized representation. 
Our approach is related to the work by Wirth et al.~\cite{chp14wirth2011,chp14rumpf2013} on geodesic calculus for shapes, but approaches the problem from an optimal control viewpoint.

In~\cite{chp14Azencott2010} the diffeomorphic matching is phrased as an optimal control problem, which is solved using a discretize-optimize approach. 
This method is related to the approach we will propose, but instead of matching images, in~\cite{chp14Azencott2010} surfaces and point clouds are registered. 
Another difference is the use of explicit first order Runge-Kutta methods (i.e. forward Euler discretization), whereas we are using fourth order methods, cf.~\autoref{sec:RungeKutta}, to fulfill numerical stability considerations.
%
Recently, Mang and Ruthotto~\cite{chp14Mang2017} also formulated LDDMM registration as an optimal control problem using a discretize-optimize approach. 
Specifically, they use a Gauss-Newton approach, which improves the convergence rate compared to the L-BFGS method we will apply, but requires the solution of additional linear systems. 
However, the biggest difference between our proposed approach and the approach by Mang et al.~\cite{chp14Mang2017} is that we use explicit Eulerian discretizations of the partial differential equation constraints of LDDMM via Runge-Kutta methods whereas Mang et al.~\cite{chp14Mang2017} use Lagrangian methods. 
While such Lagrangian methods are attractive as they eliminate the risk of numerical instabilities and allow for arbitrary time step sizes, they require frequent interpolations and consequentially become computationally demanding. 
Instead, our formulation operates on a fixed grid and only requires interpolations for the computation of the image distance measure. 
To avoid numerical instabilities due to too large time-steps, we investigate how an admissible step size can be chosen under easily obtainable estimations for the maximal displacement when using a fourth order Runge-Kutta method. 
To illustrate the difference in computational requirements we note that although in~\cite{chp14Mang2017} experiments were performed on a compute node with 40 CPUs, the computation for a 3D registration (with a number of voxels per image that is only about 10~\% of the voxel numbers of the images we are registering) takes between 25 and 120 minutes and thus is two to five times slower than our proposed methods on a single CPU. 
This dramatic difference in computational effort is most likely due to the tracking of the points/particles in the Lagrangian setting or the required interpolations.

An optimal solution, $v^*$, fulfills the optimality conditions (e.g., \eqref{eq:OptimalityConditionsHart}). 
However, these conditions need to be determined numerically. 
The classical approach, proposed by~\cite{chp14Beg2005} is the so-called {\it relaxation} approach. 
Here, for a given, $v$, one solves the transport equation for $I$ forward in time and the adjoint equation for $\lambda$ backward in time. 
Given $I$ and $\lambda$ one can compute the gradient of the energy with respect to the velocity $v$ at any point in space and time, i.e.,
\begin{equation}
  \nabla_{v_t} \mathcal{E}(v,I) = L^\dagger Lv_t+\lambda_t\nabla I_t,
\end{equation}
which can then for example be used in a gradient descent solution scheme. 
In practice, one typically uses the Hilbert-gradient~\cite{chp14Beg2005}
\begin{equation}
\label{eq:HilbertGradient}
  \nabla_{v_t}^H\mathcal{E}(v,I) = v_t + (L^\dagger L)^{-1}(\lambda_t \nabla I_t)
\end{equation}
to improve numerical convergence\footnote{Note that $(L^\dagger L)^{-1}$ amounts to a smoothing operation, and hence the Hilbert gradient is a spatially smoothed gradient.}.
Upon convergence a relaxation approach will fulfill the optimality conditions, which describe a geodesic path~\cite{chp14Beg2005}. 
Unfortunately, the optimality conditions will in practice frequently not be fulfilled exactly and the relaxation approach requires optimization over the full spatio-temporal velocity field $v$, whereas the geodesic path is completely specified by its initial conditions, i.e., its initial velocity, $v_0$, and its initial image, $I_0$. 
This has motivated the development of \textit{shooting} approaches~\cite{chp14ashburner2011,chp14vialard2012}, where one optimizes over the initial conditions of the geodesic directly instead of $v$. 
The numerical solution is similar to the relaxation approach in the sense that the adjoint system is derived to compute the gradient with respect to the initial conditions through a forward-backward-sweep. 
Shooting approaches also allow for extensions to the LDDMM registration models, such as LDDMM-based geodesic regression~\cite{chp14niethammer2011}. 

A common problem with LDDMM is its large memory footprint and the high computational costs. 
For example, to register lung CT images run times of up to three hours on a computer with 128 GB of RAM and 32 CPUs have been reported~\cite{chp14Sakamoto2014}. 
In particular for the shooting approaches several ideas to overcome these problems have been proposed. 
Marsland and McLachlan~\cite{chp14Marsland2007} as well as Durrleman et al.~\cite{chp14Durrleman2011} use a limited number of control points for LDDMM and observe that the number of control points can be decreased substantially (i.e.\ much fewer control points than number of pixels or voxels are needed) without greatly impacting the registration result. 
Zhang et al.~\cite{chp14Zhang2015} exploit the smoothness of the velocities in the Fourier domain. 
The underlying idea is that the initial velocities of the geodesic shooting are band-limited and therefore can be approximated well by a limited number of elements of a finite-dimensional vector space.

In contrast to the brain MRI or synthetic examples discussed in most  LDDMM publications, see e.g.~\cite{chp14Beg2005,chp14Durrleman2011,chp14Zhang2015}, lung registration (our motivating registration application) requires aligning many small, spatially distributed, salient structures (such as vessels). 
Hence, the spatial discretization for the images cannot be too coarse as it would otherwise risk ignoring these fine structures. 
We can still reduce computational costs and memory requirements by recognizing that velocity fields will be smooth. 
Specifically, as we assume that the structures of interest are well dispersed over the whole lung volume, we employ regular grids for the velocity fields (or momenta fields). 
However, as confirmed in our own previous work~\cite{chp14PolzinEtAl2016}, velocity fields can be discretized more coarsely (about one quarter of resolution per dimension) than the images themselves due to their inherent smoothness. 
We thereby obtain a method that aligns images well without losing accuracy compared to higher-resolution approaches while reducing computational costs and memory requirements.


%==============================================================================
\end{section}
%==============================================================================
