\def\thesection{\arabic{chapter}.A}
\section{Stability Analysis of the Transport Equation}
\label{sec:StabilityAnalysis}
\numberwithin{equation}{section}
Here we briefly investigate the stability of explicit Runge-Kutta methods. 
In particular, we focus on their use for the numerical solution of the transport equation. 
Consider applying a Runge-Kutta method to the scalar model equation 
\begin{equation}
  x' = \lambda x.
\end{equation}
This yields (using von-Neumann-Stability analysis)
\begin{equation}
\label{eq:ResultVonNeumannAnalysis}
  x^1 = R(h_t\lambda) x^0,
\end{equation}
where
\begin{equation}
\label{eq:gain_function}
R \colon \C \to \C, ~R(z) = 1+ \sum_{k=1}^s \frac{z^k}{k!}
\end{equation}
is called the stability function~\cite{Hairer1996}, which represents the gain for a particular $z=h_t\lambda$. 
Stability is achieved for $|R(z)|\leq 1$. 
Of particular interest for us, when discretizing spatial derivatives with central differences, is the stability function for the imaginary axis, i.e.\ $R(\imath y)$ with $y \in \R$. 
Having a stability region which contains part of the imaginary axis is essential as the derivative operator for central differences has eigenvalues only on the imaginary axis, as shown in the following \footnote{Note that at this point of the discretization, we consider space, but not time to be discretized. Time integration will be performed by Runge-Kutta integration, so a suitable Runge-Kutta integrator needs to contain a sufficiently large portion of the imaginary axis in its stability region.}. 
\autoref{fig:StabilityRegions} shows the stability regions of the Euler forward and backward scheme as well as of the 4-th order Runge-Kutta method. 
Clearly, for small $|\imath y|$ the 4-th order Runge-Kutta method is stable, but the Euler forward method will always be unstable.

%\textcolor{red}{@MN: Is this necessary or shall we delete this? and~\ref{fig:norm_of_imaginary_gain} show the value of $R(\imath \omega)$ for the same types of Runge-Kutta integrators. We observe, that of all considered integrators only the implicit midpoint method maps the imaginary axis to the unit circle and hence preserves energy. Explicit Runge-Kutta methods of order 3 and 4 are mildly dissipative for moderate values of $\omega$.}

In 1D, the stability of the Runge-Kutta integration of the transport equation depends on the eigenvalues of the matrix given in~\eqref{eq:CentralFiniteDifferenceMatrixNeumannNodal} which is used in the discrete right-hand side function~\eqref{eq:RightHandsideTransportMaps}. 
Let us first assume that the velocity vector $\v \in \R^n$ is constant: $v^1 = v^2 = \dots =v^n$. 
The eigenvalues of $\diag(\v) \mat{D}_1^v$ can then be computed by solving $\chi_{\mat{D}_1^v}(\lambda) = 0$:
\begin{align*}
\chi_{\mat{D}_1^v}(\lambda) &= \det(\mat{D}^v_1 - \lambda \mat{E}_n)  \\
&=\lambda^2 \det\left(\underbrace{\begin{pmatrix}
					-\lambda        & \frac{1}{2h_{x_1}^v}      		&        		& \\
					-\frac{1}{2h_{x_1}^v}       		& -\lambda      & \frac{1}{2h_{x_1}^v}      		& \\
					& \ddots 		& \ddots 		& \ddots 		& \\
					&        		& -\frac{1}{2h_{x_1}^v}     		& -\lambda      & \frac{1}{2h_{x_1}^v} \\
					&		 		& 		  		& -\frac{1}{2h_{x_1}^v}     		& -\lambda
\end{pmatrix}}_{=:\mat{A}_\mathrm{T}\in \R^{n-2 \times n-2}} \right)	
\end{align*}

The matrix $\mat{B}_\mathrm{T} := \mat{A}_\mathrm{T} + \lambda \mat{E}_n$ is a tridiagonal Toeplitz matrix with entries $a=0$, $b = \tfrac{1}{2h_{x_1}^v}$ and $c = \tfrac{-1}{2h_{x_1}^v}$. 
Eigenvalues of such matrices are~\cite{Noschese2013}
\begin{align*}
\lambda(\mat{B}_\mathrm{T}) &= a + 2 \sqrt{bc} \cos\left(\frac{k \pi}{n-1}\right) \\
&= 2 \sqrt{-\frac{1}{4(h_{x_1}^v)^2}} \cos\left(\frac{k \pi}{n-1}\right)  = \frac{\imath}{h_{x_1}^v}\cos\left(\frac{k \pi}{n-1}\right).
\end{align*}
The eigenvalues of $\mat{D}_1^v$ are thus purely imaginary (or zero). The upper bound for the spectral radius can be obtained from the fact that $|\cos (\tfrac{k \pi}{n-1})| \leq 1$:
\begin{equation}
\label{eq:spectralRadiusCentralDifferenceMatrixNeumannNodal}
\rho(\mat{D}_1^v) \leq \frac{1}{h_{x_1}^v}.
\end{equation} 
Actually, we are interested in $\rho(\diag(\v) \mat{D}_1^v)$: It is maximal if each component of $\v$ is set to its biggest absolute value: $\tilde{v}^k = \|v\|_\infty=:a_1,~k=1,\dots,n$. 
Then it holds:
\begin{equation}
\label{eq:spectralRadiusTransportIterationMatrixNeumannNodal}
\rho(\diag(\v) \mat{D}_1^v) \leq \rho(\diag(\tilde{\v}) \mat{D}_1^v) = \rho(a_1 \mat{D}_1^v) \leq \frac{a_1}{h_{x_1}^v}.
\end{equation}
%
Based on the modulus of the gain function for imaginary arguments it follows that for $y \in [-2 \sqrt{2},2 \sqrt{2}]$ the 4-th order Runge-Kutta scheme is stable, i.e. $|R(\imath y)| \leq 1$, cf.\ \autoref{fig:StabilityRegions}. 
Due to \eqref{eq:ResultVonNeumannAnalysis} we should choose $h_t$ such that:
 \begin{equation}
 \label{eq:ChoosingTimeStep1DTransport}
 h_t\frac{a_1}{h_{x_1}^v} \leq 2\sqrt{2} \overset{a_1 > 0}{\Leftrightarrow } h_t \leq \frac{2\sqrt{2} h_{x_1}^v}{a_1}.
\end{equation}  
%
To show stability in the multi-dimensional case, we exploit the following property of the Kronecker product~\cite{Zhang2013}:
\begin{equation}
\label{eq:PropertyKroneckerProduct}
\left(\bigotimes_{i=1}^n \mat{A}_i\right)  \left(\bigotimes_{i=1}^n \mat{B}_i\right) = \bigotimes_{i=1}^n (\mat{A}_i \mat{B}_i).
\end{equation}
for matrices $\mat{A}_1,\dots,\mat{A}_n,\mat{B}_1,\dots,\mat{B}_n$ of appropriate sizes. 

In~\eqref{eq:RHSTransportMaps3D}, a sum with the matrix products of the partial derivative matrices $\bar{\mat{D}}^v_i,~i=1,2,3$ is multiplied with the velocities and applied to the transformation maps. 
Again, we use the upper bounds for the velocity components $a_i = \|\v^i\|_\infty,~i=1,2,3$. 
Let $\mat{w}^i_1,\mat{w}^j_2,\mat{w}^k_3$ denote the eigenvectors for $\mat{D}^v_1,\mat{D}^v_2,\mat{D}^v_3$ with the eigenvalues $\lambda^i_1,\lambda^j_2,\lambda^k_3$, respectively. 
The following computation (where we use~\eqref{eq:PropertyKroneckerProduct}) yields an upper bound for the spectral radius of the right-hand side matrix:
\begin{align*}
&\left(\sum_{i=1}^3 a_i \bar{\D}_i^v\right) (\mat{w}^k_3 \otimes \mat{w}^j_2 \otimes \mat{w}^i_1) \\
&=(a_1 \mat{E}_{n_3} \otimes \mat{E}_{n_2} \otimes \mat{D}^v_1 + a_2 \mat{E}_{n_3} \otimes \mat{D}^v_2 \otimes \mat{E}_{n_1} \\
&+ a_3 \mat{D}^v_3 \otimes \mat{E}_{n_2} \otimes \mat{E}_{n_1} ) (\mat{w}^k_3 \otimes \mat{w}^j_2 \otimes \mat{w}^i_1) \\
&= a_1 \mat{w}^k_3 \otimes \mat{w}^j_2 \otimes (\lambda_1^i \mat{w}^i_1) + a_2 \mat{w}^k_3 \otimes (\lambda^j_2 \mat{w}^j_2) \otimes \mat{w}^i_1 \\
&+(\lambda^k_3 \mat{w}^k_3) \otimes \mat{w}^j_2 \otimes \mat{w}^i_1 \\
& = (a_1 \lambda_1^i + a_2 \lambda_2^j + a_3 \lambda_3^k)(\mat{w}^k_3 \otimes \mat{w}^j_2 \otimes \mat{w}^i_1)
\end{align*}
From~\eqref{eq:spectralRadiusCentralDifferenceMatrixNeumannNodal} we can deduce, that $\max_i |\lambda_1^i| \leq \tfrac{1}{h_{x_1}^v}$, $\max_j |\lambda_2^j| \leq \tfrac{1}{h_{x_2}^v}$, $\max_k |\lambda_3^k| \leq \tfrac{1}{h_{x_3}^v}$ and hence the spectral radius of the right-hand side matrix has the following upper bound:
\begin{equation}
\label{eq:SpectralRadiusTransportIterationMatrix3D}
\rho\left(\sum_{i=1}^3 \diag(\v^i_k)\bar{\D}_i^v \right) \leq \frac{a_1}{h_{x_1}^v} + \frac{a_2}{h_{x_2}^v} + \frac{a_3}{h_{x_3}^v}
\end{equation}
The straightforward extension of~\eqref{eq:ChoosingTimeStep1DTransport} yields that the Runge Kutta scheme is stable, if
\begin{equation}
h_t \leq 2 \sqrt{2}\left(\sum_{i=1}^3\frac{a_i}{h_{x_i}^v} \right)^{-1}
\end{equation}
for a chosen $a_i > 0$.

\begin{figure}
\centering
 \begin{tabular}{ccc}
  \includegraphics[width = 2.4cm]{fig/stability_fe_cropped.pdf} &  \includegraphics[width = 2.4cm]{fig/stability_be_cropped.pdf} &  \includegraphics[width = 2.4cm]{fig/stability_rk4_cropped.pdf} \\
  Forward Euler & Backward Euler & Runge-Kutta 4
 \end{tabular}
 \caption{\label{fig:StabilityRegions}Stability regions for the Forward Euler, Backward Euler and the 4-th oder Runge-Kutta methods marked in gray. As we are using central differences in space, the imaginary axis is of particular interest.}
\end{figure}
