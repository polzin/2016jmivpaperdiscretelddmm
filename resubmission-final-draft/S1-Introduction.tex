%------------------------------------------------------------------------------
\begin{section}{Introduction}\label{sec:introduction}
%------------------------------------------------------------------------------


  The goal of image registration is to establish spatial correspondences between images. Image registration is a challenging, but important task in image analysis. In particular, in medical image analysis, image registration is a key tool to compare patient data in a common space, to allow comparisons between  pre-, inter-, or post-intervention images, or to fuse data acquired from different and complementary imaging devices such as positron emission tomography (PET), computed tomography (CT), or magnetic resonance imaging (MRI)~\cite{MaiVie1998}. 

Image registration is of course not limited to medical imaging, but is important for a wide range of applications: For example, it is used in astronomy, biology/genetics, cartography, computer vision and surveillance. Consequently, there exists a vast number of approaches and techniques; see, e.g., 
\cite{%
Brown1992,%
2008-IP-FM,%
FitHilMau2000,%
2012-Goshtasby,%
HajHawHil2001,%
MaiVie1998,%
modersitzki2004,%
modersitzki2009,%
2015-HMMI-RM,%
Scherzer2006,%
2013-TMI-SDP,%
ElsPolVie1993,%
2003-ZF%
}
and references therein.
	
Optical flow approaches~\cite{Horn1981,Lucas1981} are popular and commonly used in computer vision~\cite{Bruhn2005,2005-WBBP,Papenberg2006,Zach2007,Brox2011}. 
However, computer vision applications typically have different requirements regarding spatial transformations than medical image analysis applications. 
For example, when processing natural images, objects, such as cars or people, should typically be allowed to move independently of the background. However, in many medical applications it is natural to constrain the sought-for mapping to be bijective or even diffeomorphic. 
In early days of image registration, transformation models were (and sometimes still are) constricted to rigid mappings, which obviously fulfill this constraint. 
However, more flexible, e.g., deformable, transformation models are required to capture subtle localized changes. 
Early work on deformable image registration includes variational approaches based on elasticity theory~\cite{FisEls1973,Broit1981,BajLieRei1983} where a spatial transformation is represented non-parametrically (via a deformation field) and is regularized through an elastic potential function.

While the mathematical reason for introducing the regularizer is to ensure solutions of the variational formulation, it may also be interpreted as a penalty for non-elasticity. 
Elastic registration has been widely and successfully used. 
However, an elastic regularizer can in general not guarantee that a computed mapping is bijective~\cite{ChrRabMill1996}. 
To assure bijectivity one may add constraints to a registration formulation. 
Alternatively, one can formulate a registration model which by construction guarantees the regularity of the transformation. 
Examples for the former approach are~\cite{RohlfingEtAl2003,2004-IP-HM,2006-IJCV-HM}.
While Rohlfing et al.~\cite{RohlfingEtAl2003} employed an additonal penalty term on the determinant of the Jacobian of the transformation that improves transformation regularity, but cannot guarantee bijective mappings, Haber and Modersitzki developed similar ideas, but with equality~\cite{2004-IP-HM} or box constraints~\cite{2006-IJCV-HM} thereby guaranteeing bijectivity. 
Nevertheless, Haber and Modersitzki still use a mathematical model which is based on linear elasticity theory which is only appropriate for small deformations. 
As convex energies, such as the linear elastic potential, result in finite penalties they are insufficient to guarantee one-to-one maps~\cite{2013-BMR-SISC,Ciarlet1988}. 
Therefore, Burger et al.~\cite{2013-BMR-SISC} used hyper-elasticity and quasi convex functions to obtain a registration model yielding bijective transformations while allowing for large deformations.
	


Another possibility to assure diffeomorphic mappings (smooth mappings which are bijective and have a smooth inverse) is to express a transformation implicitly through velocity fields. 
The intuitive idea is that it is easy to assure diffeomorphic transformations for small-scale displacements by using a sufficiently strong spatial regularization. 
Hence, one may obtain a complex diffeomorphic transform, capturing large displacements, as the composition of a large or potentially infinite number of small-scale diffeomorphisms. 
These small-scale diffeomorphisms in turn can be obtained by integrating a static or time-dependent velocity field in time. 
Early work explored such ideas in the context of fluid-based image registration~\cite{ChrRabMill1996}. 
In fact, for approaches using velocity fields the same regularizers as for the small displacement registration approaches (based on elasticity theory or the direct regularization of displacement fields) can be used. 
However, the regularizer is now applied to one or multiple velocity fields instead of a displacement field and the transformation is obtained via time-integration of the velocity field.

The solution of the arising problems is computationally expensive, as, in the most general case, one now needs to estimate a spatio-temporal velocity field instead of a static displacement field. 
Greedy solution approaches as well as solutions based on a stationary velocity field have been proposed to alleviate the computational burden (both in computational cost, but also with respect to storage requirements)~\cite{ChrRabMill1996,Arsigny2006,Ashburner2007,Mang2015}. 
However, these methods do in general not provide all the nice mathematical properties (metric, geodesics, etc.) of the non-greedy large deformation diffeomorphic metric mapping (LDDMM) registration approach~\cite{Miller2002} which we will explore numerically here.

Various numerical approaches have been proposed for LDDMM, e.g.~\cite{ChrRabMill1996,Miller2002,TwiMar2004,Beg2005}. 
Traditionally, relaxation formulations~\cite{Beg2005} have been used to solve the LDDMM optimization problem. 
Here, one optimizes directly over spatio-temporal velocity fields, assuring that the velocity field at any given time is sufficently smooth.
The resulting transformation, that is obtained via time-integration of the spatio-temporal velocity field, is used to deform the source image such that it becomes similar to the target image. 
Specifically, the LDDMM relaxation formulation is solved via an optimize-discretize approach~\cite{Beg2005}, where a solution to the continuous time optimality conditions for the associated constrained optimization problem (the constraint being the relation between the spatial transformation and the velocity fields) is computed. 
These optimality conditions are the Euler-Lagrange equations corresponding to the constrained optimization problem and can be regarded as the continuous equivalent of the Karush-Kuhn-Tucker (KKT)~\cite[p.~321]{nocedal2006} equations of constrained optimization. 
To numerically determine a solution fulfilling the Euler-Lagrange equations one uses an adjoint solution approach which allows the efficient computation of the gradient of the LDDMM relaxation energy with respect to the spatio-temporal velocity field via a forward/backward sweep. 
This gradient can then be used within a gradient descent scheme or as the basis of sophisticated numerical solution approaches. 
Note that the adjoint solution approach is for example also at the core of the famous backpropagation algorithm for the training of neural networks~\cite{lecun-88} or the reverse mode of automatic differentiation~\cite[pp.~37]{griewank2008}\footnote{Automatic differentiation is heavily used in modern appproaches for deep learning to avoid manual computations of adjoint equations altogether.}. For an LDDMM relaxation solution, a geodesic path (fulfilling the Euler-Lagrange equations exactly) is only obtained at convergence~\cite{Beg2005}.

More recently, shooting approaches like~\cite{ashburner2011,vialard2012,niethammer2011} have been proposed. Again, an optimize-discretize approach is used. 
Here, instead of numerically solving the Euler-Lagrange equations of the LDDMM energy (which can alternatively be represented via the Euler-Poincar\'{e} Diffeomorphism equation~\cite{Miller2006} (EPDiff)), these Euler-Lagrange equations are imposed as a dynamical system and the LDDMM energy is reformulated as an initial value problem. 
Hence, all paths obtained during the optimization process are by construction geodesics -- they may just not be the optimal ones. 
The price to pay for such a reduced parameterization is that one now optimizes over a second order partial differential equation which describes the geodesic path. 
A simple analogy in the one-dimensional Euclidean space is that in relaxation, registration between two points is performed over all possible paths leading at convergence to a straight line path. 
On the contrary, for shooting one already knows that the optimal solution should be a straight line and consequentially optimization is only performed regarding the line's slope and y-intercept.

Although the mathematical framework for LDDMM is very appealing, its numerical treatment is not trivial, may lead to non-diffeomorphic transformations, and is highly demanding both in terms of memory and computational costs. 
As discussed above, current LDDMM solutions are mainly based on optimize-discretize formulations, i.e., optimality conditions are derived in continuous space and then discretized~\cite{Beg2005,Miller2006}. 
Not surprisingly, it is possible that a solution of the discrete equations is neither an optimizer for a discrete nor the continuous energy~\cite{haber2007model}. 

Hence, the goal of this chapter is to develop a discretize-optimize approach for LDDMM, where the starting point is the discretization of the \textit{energy functional}. 
Consequentially, solutions to the associated optimality conditions are indeed optimizers of the discretized energy. 
Additionally, we integrate a suitable interpolation operator~\cite{Koenig2014} into the LDDMM framework to reduce computational demands and memory consumption without losing registration accuracy. 
Furthermore, we use the so-called Normalized Gradient Fields (NGF) distance measure, which is designed to align image edges~\cite{Haber2007,modersitzki2009}. 
NGF has been successfully applied to lung CT registration~\cite{Ruehaak2013,Koenig2014,2013-PIA-PRWSHHM,Polzin2014,PolzinEtAl2016,Ruehaak2017}, which is one of our example applications in~\autoref{sec:experiments}.

The chapter is organized as follows. 
In~\autoref{sec:background} the LDDMM concept is introduced and two approaches~(\cite{Beg2005,Hart2009}) used for solving the constrained optimization problems are discussed. 
In~\autoref{sec:ContinuousModels} we extend these models, discuss the shooting approach, and formulate them in the context of general distance measures for images.
For simplicity, \autoref{sec:Discretization1D} discusses the discretization of the models in the one-dimensional case. 
This allows introducing the basic ingredients (grids, regularization, derivatives, transport equation, etc.) required for the 3D formulation in a compact manner. 
In~\autoref{sec:PDESolution} we address the discretization and solution of the partial differential equations constraining the LDDMM models (e.g., the transport equations) via Runge-Kutta methods. 
The extension of the described formalism to 3D is given in~\autoref{sec:Discretization3D}.
In~\autoref{sec:MultilevelAndNumericalOptimization} details on the numerical optimization and the solution of the optimization problems in a multilevel approach are provided. 
Afterwards, in~\autoref{sec:experiments} the performance of the proposed methods is evaluated and experimental results are presented. 
Finally, we conclude the chapter with a discussion of the results and possible extensions in~\autoref{sec:discussion}.

%------------------------------------------------------------------------------
\end{section}
%------------------------------------------------------------------------------
